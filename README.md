![](https://i.imgur.com/dMPfzBQ.png)

## Developer Challenge

The objective of this challenge is to hit an endpoint containing the list of job postings and perform some actions on the result.

This endpoint containing a json list of jobs listings [https://api.myjson.com/bins/kez8a](https://api.myjson.com/bins/kez8a)

## Requirements and Output

#### Create a RESTful API to **allow search** in the given jobs by any of the following:

- jobtitle
- companyname
- location [CITY] 
- skills [C,Nodejs]
- source [URL]
- salary
- type
- experience
- startdate and enddate [ex: 10-10-2020:07-10-2020]

and allow sorting by:

- location
- type

####  Build a frontend application to consume your final search endpoint using one of the following:
- Angular
- React
- Vuejs


## Conditions

- Fetch the data directly from the URL and not create a JSON file
- You can use PHP >= 7.1 or NodeJs =>9.x or both to accomplish this task
- Write Unit tests for both Frontend & Backend application
- Do not use any database or full text search engines
- If you make any assumptions while solving the exercise please mention them clearly in the readme file

## What we are looking for

- **Simple, clear, readable code** How well structured it is? Clear separation of concerns? Can anyone just look at it and get the idea to
what is being done? Does it follow any standards?
- **Correctness** Does the application do what it promises? Can we find bugs or trivial flaws?
- **Memory efficiency** How will it behave in case of large datasets?
- **Testing** How well tested your application is? Can you give some metrics?

## Questions & Delivery

If you have any questions to this challenge, please do reach out to us.

The challenge should be delivered as a link to a public git repository (github.com or bitbucket.com or gitlab are preferred).

## Checklist

Before submitting, make sure that your program

- [ ] Code accompanies the Unit Tests
- [ ] Usage is clearly mentioned in the README file, This including setup the project, how to run it, how to run unit test, examples,etc
- [ ] Uses the endpoint directly

## Note

Implementations focusing on **quality over feature completeness** will be highly appreciated,  don’t feel compelled to implement everything and even if you are not able to complete the challenge, please do submit it anyways.
